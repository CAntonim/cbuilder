#include "buildHelper.hpp"

build_helper* build_helper::instance = 0;

build_helper::build_helper()
{
}

/*
returns a the instance of this object used to create the singleton
design pattern
*/
build_helper* build_helper::getInstance()
{
	if(instance == NULL)
	{
		instance = new build_helper;
	}
	return instance;
}

//returns if an internal flags is set
bool build_helper::isFlagSet(const std::string& flag)
{
	if(build_flags.count(flag) < 1 ){
		return false;
	}
	return true;
}

//returns the value of a flag
bool build_helper::getFlag (const std::string& flag)
{
	std::map<std::string,bool>::const_iterator position =
		build_flags.find(flag);

	if (position != build_flags.end())
	{
		return position->second;
	}
	else
	{
		printError("read access vailoation tried to access " +
				   flag + "which was not set");
		throw;
	}
}

//sets the value of a flags
void build_helper::setFlag (const std::string& flag, const bool& value)
{
	if(isFlagSet(flag))
	{
		build_flags.at(flag) = value;
		return;
	}
	build_flags.insert(std::make_pair(flag,value));
	return;
}

//ensures that a flag in the programe has been unset
void build_helper::unsetFlag (const std::string& flag)
{
	if(isFlagSet(flag))
	{
		build_flags.erase(flag);
		return;
	}
	return;
}

//returns if there is a log file that has been set
bool build_helper::isLogFilePathSet()
{
	if(log_file_path == NULL)
	{
		return false;
	}
	return true;
}

//This sets the internal value of the path to the log file
void build_helper::setLogFilePath(const std::string& path)
{
    if (isLogFilePathSet())
    {
        * log_file_path = path;
        return;
    }
    log_file_path = new std::string (path);
    return;
}

std::string build_helper::getLogFilePath()
{
    return *log_file_path;
}

//this will return the string to color a text such that each
std::string build_helper::color_text (color_codes_fg fg_color,
									  color_codes_bg bg_color,
									  std::string text)
{
	//anis set color marker
	std::string prefix = "\033[";
	//ansi reset code
	std::string suffix = "\033[0m";

	if (!(fg_color == FG_DEFAULT))
	{
		prefix += fg_color;
	}
	//forground background seperator
	prefix += ";";

	if (bg_color != BG_DEFAULT)
	{
		prefix += bg_color;
	}
	//color code terminating charicter
	prefix += "m";

	return prefix + text + suffix ;
}

void build_helper::printInfo (const std::string& message)
{
	if (isFlagSet("verbose"))
	{
		if(getFlag("verbose"))
		{
			std::cout << "["
			<< color_text(FG_BLUE,BG_DEFAULT,"INFO")
			<< "] : " << message << "\n";
		}
		return;
	}
	printError("Verbose flag has not been set");
	exit(SIGTERM);
}

void build_helper::printStatus (const std::string& message)
{
	std::cout << "["
		<< color_text(FG_GREEN,BG_DEFAULT,"STATUS")
		<< "] : " << message << "\n";
}

void build_helper::printWarning (const std::string& message)
{
	std::cout << "["
		<< color_text(FG_YELLOW,BG_DEFAULT,"WARNING")
		<< "] : " << message << "\n";
}

void build_helper::printError (const std::string& message)
{
	std::cout << "["
		<< color_text(FG_YELLOW,BG_DEFAULT,"ERROR")
		<< "] : " << message << "\n";
}
