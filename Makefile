all: build

CPLUSFLAGS = $(WARNINGS) $(DEBUG) $(OPTIMIZE) $(Includes) $(Linkage)
WARNINGS = -Wall
DEBUG = -ggdb -fno-omit-frame-pointer
OPTIMIZE = -O2
Includes = -I.
Linkage = -L.

build: build.cpp buildHelper.cpp
	$(CXX) --static $(CPLUSFLAGS) -o $@ build.cpp buildHelper.cpp


clean:
	rm -f build

# Builder will call this to install the application before running.
install:
	echo "Installing is not supported"

# Builder uses this target to run your application.
run:
	./build

