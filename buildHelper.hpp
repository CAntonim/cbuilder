# ifndef __BUILDER_HELPER__
# define __BUILDER_HELPER__

#include <map>
#include <iostream>
#include <string>
#include <iterator>
#include <csignal>
/*
	A helper object for handling the config properties of the applications
	use this for logging and handling system flags only. It follow the
	singleton design pattern so that there is only ever 1 copy of this
	object at any time.
*/
class build_helper
{
	private:
		//Config values
		std::map <std::string,bool> build_flags;
		std::string* log_file_path = NULL;
		//Singleton values
		static build_helper* instance;
		//log file path
		build_helper();

		enum color_codes_fg
		{
			FG_BLACK	=30,
			FG_RED		=31,
			FG_GREEN	=32,
			FG_YELLOW	=33,
			FG_BLUE	=34,
			FG_MAGENTA	=35,
			FG_CYAN	=36,
			FG_WHITE	=37,
			FG_DEFAULT	=-1
		};

		enum color_codes_bg
		{
			BG_BLACK	=40,
			BG_RED		=41,
			BG_GREEN	=42,
			BG_YELLOW	=43,
			BG_BLUE	=44,
			BG_MAGENTA	=45,
			BG_CYAN	=46,
			BG_WHITE	=47,
			BG_DEFAULT	=-1
		};

	public:
		/*
		returns a the instance of this object used to create the singleton
		design pattern
		*/
		static build_helper* getInstance();
		//returns if an internal flags is set
		bool isFlagSet (const std::string & flag);
		//returns the value of a flag
		bool getFlag (const std::string & flag);
		//sets the value of a flags
		void setFlag (const std::string& flag,
					  const bool& value);
		//ensures that a flag in the programe has been unset
		void unsetFlag (const std::string& flag);
		//returns if there is a log file that has been set
		bool isLogFilePathSet ();
		//This sets the internal value of the path to the log file
		void setLogFilePath (const std::string& path);
		//returns the value of the of log file path throws exception if its not
		//set
		std::string getLogFilePath ();
		//this will return the string to color a text such that each
		std::string color_text	(color_codes_fg fg_color,
								 color_codes_bg bg_color,
								 std::string text);
		// This prints infomation to the screen about how far along the build
		void printInfo (const std::string& message);
		/*
		prints the current status of the build
		*/
		void printStatus (const std::string& message); // not implemented yet
		//prints an error message out to standard
		void printError (const std::string& message);
		/*
		prints infomation about potental erros with the system that not
		problomatic enough to stop the running of the application
		*/
		void printWarning (const std::string& message);
};

#endif

